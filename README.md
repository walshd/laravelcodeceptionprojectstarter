### What is this repository for? ###

This is a bootstrap for Laravel 5.1 projects with CodeCeption set up.  

### How do I get set up? ###

**Installing**

*Acquire a copy*

There are multiple ways to acquire a copy of LaravelCodeceptionProjectStarter. You can download a ZIP archive, clone the project and finally fork your own repository then clone it.

*Download*

To download a ZIP archive, simply got to the main repository page at of LaravelCodeceptionProjectStarter and click on the "Download ZIP" button.

*Clone*

Simply clone this repository on your machine using the command:

    git clone https://walshd@bitbucket.org/walshd/laravelcodeceptionprojectstarter.git LaravelCodeceptionProjectStarter

*Fork & Clone*

Finally the recommended method would be to first fork this repository then clone your own repository. Follow this guide to learn how to fork an existing repository. Then use a command similar to this to clone your own repository:

    git clone https://walshd@bitbucket.org/walshd/laravelcodeceptionprojectstarter.git LaravelCodeceptionProjectStarter

**Composer**

Fetch all dependencies using composer by issuing the following command:

    composer install

**Node.js**

Fetch all dependencies for Node.js using npm by using the following command:

    npm install

**Basic configuration**

*Create your .env file*

Create a .env file from the .env.example supplied.

    cp .env.example .env

*Generate application key*

Generate the unique application key:

    ./artisan key:generate


**Final Checks**

Once all dependencies have loaded carry out some basic checks:

1) Run 'php artisan serve' in a terminal window (ensuring you are in the project folder before running the command) - then open a  browser and check you see the laravel logo when visiting 'http://localhost:8000'. You can stop the server if you see this. (ctrl+c or cmd+c).

2) run 'phpunit' command and ensure you get back 'OK (1 test, 2 assertions)'.

3) create a local database using the 'create db on linux distro' tutorial on WTE.

4) when running the CodeCeption command if there is no CURL installed then run the following commands:
    
     sudo php5enmod curl
    
     sudo apt-get update
     sudo apt-get install ehu-webdev

Now you are ready to start the project build! 

### Who do I talk to? ###

Dave Walsh - david.walsh@edgehill.ac.uk